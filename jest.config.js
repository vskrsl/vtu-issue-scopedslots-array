module.exports = {
	moduleFileExtensions: [
		'js',
		'ts',
		'tsx',
	],
	testRegex: [
		/__tests__\/[\w-]+\.test\.tsx?$/,
	],
	transform: {
		'\\.(t|j)sx?$': require.resolve('babel-jest'),
	},
	transformIgnorePatterns: ['/node_modules/'],

	testEnvironment: require.resolve('jest-environment-jsdom-sixteen'),
}
