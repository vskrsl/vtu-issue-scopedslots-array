import { Wrapper, mount } from '@vue/test-utils'
import { Tag } from '../tag'

let wrapper: Wrapper<Vue>

const findAllVSlotElement = () => wrapper.findAll('#v-slot')

describe('ScopedSlot.myScoped as Array', () => {
	afterEach(() => {
		wrapper.destroy()
	})
	
	it('dont work as expected', () => {
		wrapper = mount(Tag, {
			scopedSlots: {
				myScoped() {
					return [
						<div id="v-slot">Click in this must return 0 in console</div>,
						<div id="v-slot">Click in this must return 1 in console</div>
					]
				}
			}
		})

		expect(findAllVSlotElement().length).toBe(2)
		
		/**
		 * I expected two elements. But got one. Okay.
		 * 
		 * From docs we got: "Root Element required". 
		 * But in my <Tag> logic i need to wrap every recieved scoped slot, because i need onClick event with index.
		 * 
		 * From docs we got: "the recommended workaround is to wrap the component under test in another component and mount that one". 
		 * But if i make wrapComponent i will always get 0 in my onClick whatever slot element was clicked..
		 */ 
	})

	it('work as expected', () => {
		/** I can solve this case with native Vue: */

		const dummyComponent = {
			template: `
			<Tag>
				<template 
					v-slot:myScoped="props"
				>
					<div
						id="v-slot"
						v-for="(element, i) of myScoped"
					>
						{{element}}
					</div>
				</template>
			</Tag>
			`,
			components: {
				Tag
			},
			computed: {
				myScoped() {
					return ['Element 1','Element 2']
				},
			},
		}

		wrapper = mount(dummyComponent)
		expect(findAllVSlotElement().length).toBe(2)
	})
})