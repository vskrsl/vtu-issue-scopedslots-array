import Vue from 'vue'
import { Component } from 'vue-property-decorator'

@Component
export class Tag extends Vue {
	public render() {
		return (
			<div
				aria-label="scoped-slots"
			>
				{
					this.$scopedSlots?.myScoped?.({})?.map((element, i) => {
						return (
							<div
								onClick={() => console.log('fire click on index:', i)}
							>
								{element}
							</div>
						)
					}) 
				}
			</div>
		)
	}
}