const tsxOptions = {
	isTSX: true,
	allExtensions: true,
}

module.exports = (api) => ({
	presets: [
		['@babel/preset-typescript', tsxOptions],
		'@vue/babel-preset-jsx',
		['@babel/preset-env', {
			modules: api.env('test') ? 'auto' : false,
			loose: true,
		}],
	],
	plugins: [
		['@babel/plugin-transform-typescript', tsxOptions],
		['@babel/plugin-proposal-decorators', {
			legacy: true,
		}],
		['@babel/proposal-class-properties', {
			loose: true,
		}],

		['@babel/plugin-proposal-optional-chaining', {
			loose: true,
		}],
	],

})
